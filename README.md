# NFDI4DS widget

[Visit the widget configuration page here](https://tibhannover.gitlab.io/nfdi4ds/nfdi4ds-widget)

## Local development

Installation

`npm install`

Run development mode

`npm start`

Building for production

`npm run build`

Files out outputted to the `public/` folder. The `widget.js` and image files are of interested for general use. The `index.html` is used for the [widget configuration page here](https://tibhannover.gitlab.io/nfdi4ds/nfdi4ds-widget)

## More info

We use Tailwind for styling. All classes are prefixed with `nfdi4ds-*` to prevent potential class conflicts. Also, all used classes are using the `!important` css rule, to ensure web specific CSS is overwritten.
