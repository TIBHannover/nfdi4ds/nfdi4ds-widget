import html from './widget.html';
import './widget.css';

export function show() {
    const locations = document.querySelectorAll('.nfdi4ds-banner');

    for (let i = 0; i < locations.length; i++) {
        const temporary = document.createElement('div');
        temporary.innerHTML = html;

        const widget = locations[i];

        //const variant = widget.getAttribute('data-variant');
        const buttonColor = widget.getAttribute('data-button-color');
        const menuColor = widget.getAttribute('data-menu-color');

        // Remove content in case widget is rendered already
        widget.innerHTML = '';

        // Add widget to dom
        while (temporary.children.length > 0) {
            widget.appendChild(temporary.children[0]);
        }

        /** Set button color theme */
        if (buttonColor === 'dark') {
            widget
                .querySelector('[data-id="button-container"]')
                .classList.add('nfdi4ds-dark');
        }

        /** Set dropdown menu color theme */
        if (menuColor === 'dark') {
            widget
                .querySelector('[data-id="dropdown-container"]')
                .classList.add('nfdi4ds-dark');
        }

        /** Define the elements */
        const dropdown = widget.querySelector("[data-id='dropdown']");
        const dropdownServices = widget.querySelector(
            "[data-id='dropdown-services']"
        );
        const button = widget.querySelector("[data-id='button']");
        const buttonServices = widget.querySelector(
            "[data-id='services-button']"
        );

        /** Show menu on button click */
        button.addEventListener('click', () => {
            dropdown.classList.toggle('nfdi4ds-hidden');
            button.setAttribute(
                'aria-expanded',
                !dropdown.classList.contains('nfdi4ds-hidden')
            );
        });

        function closeDropdown() {
            if (!dropdown.classList.contains('nfdi4ds-hidden')) {
                dropdown.classList.add('nfdi4ds-hidden');
                dropdownServices.classList.add('nfdi4ds-hidden');
                button.setAttribute('aria-expanded', false);
                dropdownServices.setAttribute('aria-expanded', false);
            }
        }

        /** Hide menu when clicked outside */
        document.addEventListener('click', function (e) {
            if (!dropdown.contains(e.target) && !button.contains(e.target)) {
                closeDropdown();
            }
        });

        /** Show services menu on click */
        buttonServices.addEventListener('click', function () {
            dropdownServices.classList.toggle('nfdi4ds-hidden');
            buttonServices.setAttribute(
                'aria-expanded',
                !dropdownServices.classList.contains('nfdi4ds-hidden')
            );
        });

        /** Add keyboard events for a11y reasons */
        buttonServices.addEventListener('keydown', function (e) {
            if (e.code === 'Space' || e.code === 'Enter') {
                buttonServices.click();
            }
        });

        document.addEventListener('keydown', function (e) {
            if (e.code === 'Escape') {
                closeDropdown();
            }
        });
    }
}
