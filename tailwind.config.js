module.exports = {
    content: ['./src/**/*.{html,js}'],
    theme: {
        extend: {},
    },
    plugins: [],
    prefix: 'nfdi4ds-',
    corePlugins: {
        preflight: false, // disable the default styling to prevent issues with existing webpages
    },
    important: true, // adds important to all tailwind classes
    darkMode: 'class', // control dark mode with a class instead of OS setting
};
