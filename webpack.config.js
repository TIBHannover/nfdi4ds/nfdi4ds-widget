const path = require('path');
const webpack = require('webpack');
const copyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const bundleOutputDir = './demo';
const bundleReleaseOutputDir = './public';

module.exports = (env, argv) => {
    const isDevBuild = argv.mode === 'development';

    return [
        {
            entry: './src/main.js',
            output: {
                filename: 'widget.js',
                path: isDevBuild
                    ? path.resolve(bundleOutputDir)
                    : path.resolve(bundleReleaseOutputDir),
            },
            devServer: {
                static: './',
            },
            plugins: [
                ...(isDevBuild
                    ? [
                          new webpack.SourceMapDevToolPlugin(),
                          new copyWebpackPlugin({
                              patterns: [{ from: 'demo/' }],
                          }),
                      ]
                    : [new TerserPlugin()]),
            ],
            module: {
                rules: [
                    { test: /\.html$/i, use: 'html-loader' },
                    {
                        test: /\.css$/i,
                        use: ['style-loader', 'css-loader', 'postcss-loader'],
                    },
                    {
                        test: /\.(jpe?g|png|gif|woff|woff2|eot|ttf|svg)(\?[a-z0-9=.]+)?$/,
                        resourceQuery: { not: [/raw/] },
                        type: 'asset/inline',
                    },
                    {
                        test: /\.js$/i,
                        exclude: /node_modules/,
                        use: {
                            loader: 'babel-loader',
                            options: {
                                presets: [
                                    [
                                        '@babel/env',
                                        {
                                            targets: {
                                                browsers: ['ie 6', 'safari 7'],
                                            },
                                        },
                                    ],
                                ],
                            },
                        },
                    },
                ],
            },
        },
    ];
};
